const { semanticReleaseConfigDefault } = require('@beepbeepgo/semantic-release');
module.exports = semanticReleaseConfigDefault();
